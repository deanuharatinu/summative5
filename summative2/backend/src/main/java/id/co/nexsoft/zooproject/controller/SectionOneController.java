package id.co.nexsoft.zooproject.controller;

import id.co.nexsoft.zooproject.model.SectionOne;
import id.co.nexsoft.zooproject.service.SectionOneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SectionOneController {

    @Autowired
    SectionOneService sectionOneService;

    @GetMapping("/section-one")
    public Iterable<SectionOne> getSectionOneData() {
        return sectionOneService.getSectionOneData();
    }

}
