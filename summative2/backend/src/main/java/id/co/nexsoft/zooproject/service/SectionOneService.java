package id.co.nexsoft.zooproject.service;

import id.co.nexsoft.zooproject.model.SectionOne;
import id.co.nexsoft.zooproject.repository.SectionOneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SectionOneService {

    @Autowired
    SectionOneRepository sectionOneRepo;

    public Iterable<SectionOne> getSectionOneData() {
        return sectionOneRepo.findAll();
    }
}
