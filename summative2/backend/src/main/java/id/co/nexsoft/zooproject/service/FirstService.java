package id.co.nexsoft.zooproject.service;

import id.co.nexsoft.zooproject.model.First;
import id.co.nexsoft.zooproject.repository.FirstRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FirstService {

    @Autowired
    FirstRepository firstRepo;

    public Iterable<First> getFirstData() {
        return firstRepo.findAll();
    }
}
