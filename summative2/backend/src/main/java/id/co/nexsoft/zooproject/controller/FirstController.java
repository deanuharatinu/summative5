package id.co.nexsoft.zooproject.controller;

import id.co.nexsoft.zooproject.model.First;
import id.co.nexsoft.zooproject.service.FirstService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FirstController {

    @Autowired
    FirstService firstService;

    @GetMapping("/first")
    public Iterable<First> getFirstData() {
        return firstService.getFirstData();
    }
}
