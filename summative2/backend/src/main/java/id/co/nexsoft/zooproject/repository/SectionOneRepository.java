package id.co.nexsoft.zooproject.repository;

import id.co.nexsoft.zooproject.model.SectionOne;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SectionOneRepository extends CrudRepository<SectionOne, Integer> {
}
