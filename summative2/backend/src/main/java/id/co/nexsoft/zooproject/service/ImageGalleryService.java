package id.co.nexsoft.zooproject.service;

import id.co.nexsoft.zooproject.model.ImageGallery;
import id.co.nexsoft.zooproject.repository.ImageGalleryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageGalleryService {

    @Autowired
    ImageGalleryRepository imageGalleryRepo;

    public Iterable<ImageGallery> getImageData() {
        return imageGalleryRepo.findAll();
    }
}
