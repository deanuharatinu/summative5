package id.co.nexsoft.zooproject.controller;

import id.co.nexsoft.zooproject.model.SectionTwo;
import id.co.nexsoft.zooproject.service.SectionTwoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SectionTwoController {

    @Autowired
    SectionTwoService sectionTwoService;

    @GetMapping("/section-two")
    public Iterable<SectionTwo> getSectionTwoData() {
        return sectionTwoService.getSectionTwoData();
    }

}
