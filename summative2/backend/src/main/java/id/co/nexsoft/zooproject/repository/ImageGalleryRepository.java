package id.co.nexsoft.zooproject.repository;

import id.co.nexsoft.zooproject.model.ImageGallery;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageGalleryRepository extends CrudRepository<ImageGallery, Integer>  {
}
