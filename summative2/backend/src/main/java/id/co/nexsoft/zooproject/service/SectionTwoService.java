package id.co.nexsoft.zooproject.service;

import id.co.nexsoft.zooproject.model.SectionTwo;
import id.co.nexsoft.zooproject.repository.SectionTwoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SectionTwoService {

    @Autowired
    SectionTwoRepository sectionTwoRepo;

    public Iterable<SectionTwo> getSectionTwoData() {
        return sectionTwoRepo.findAll();
    }

}
