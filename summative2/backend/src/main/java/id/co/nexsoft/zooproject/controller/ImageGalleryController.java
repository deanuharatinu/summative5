package id.co.nexsoft.zooproject.controller;

import id.co.nexsoft.zooproject.model.ImageGallery;
import id.co.nexsoft.zooproject.service.ImageGalleryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ImageGalleryController {

    @Autowired
    ImageGalleryService imageGalleryService;

    @GetMapping("/image")
    public Iterable<ImageGallery> getImageData() {
        return imageGalleryService.getImageData();
    }
}
