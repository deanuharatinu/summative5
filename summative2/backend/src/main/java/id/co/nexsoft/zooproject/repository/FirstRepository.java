package id.co.nexsoft.zooproject.repository;

import id.co.nexsoft.zooproject.model.First;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FirstRepository extends CrudRepository<First, Integer> {

}
