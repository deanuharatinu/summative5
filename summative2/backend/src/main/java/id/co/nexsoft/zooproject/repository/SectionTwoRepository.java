package id.co.nexsoft.zooproject.repository;

import id.co.nexsoft.zooproject.model.SectionTwo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SectionTwoRepository extends CrudRepository<SectionTwo, Integer>  {
}
